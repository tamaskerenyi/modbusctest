#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <aSubRecord.h>

#include <epicsTime.h>
#include <sys/times.h>
#include <unistd.h>
#include <time.h>

static int readFiles(aSubRecord *precord);

static int readFiles(aSubRecord *precord) {
    // copying value from INPA to OUTA
    precord->vala = precord->a;
    precord->valb = 26;
    precord->valc = 1;
    return(0);
    }
    //
epicsRegisterFunction(readFiles);
